package ro.pentastagiu.parkingapp.service;

import ro.pentastagiu.parkingapp.model.Ticket;
import ro.pentastagiu.parkingapp.model.VehicleType;

public interface ParkingService {
	public Ticket processVehicle(VehicleType vehicleType);

	public VehicleType typeOfVehicle(int parkingSpaceID);

	public long calculateParkingDuration(String ticketCode);

	public VehicleType findSpaceType(String ticketCode);
}
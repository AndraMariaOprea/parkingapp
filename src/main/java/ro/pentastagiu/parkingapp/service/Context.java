package ro.pentastagiu.parkingapp.service;

import java.util.Map;

import ro.pentastagiu.parkingapp.model.VehicleType;

public class Context {
	
	private PaymentStrategy paymentStrategy;
	
	public Context(PaymentStrategy paymentStrategy) {
		this.paymentStrategy = paymentStrategy; 
	}
	
	 public Map<VehicleType, Integer> executePaymentStrategy(){
		
		 return paymentStrategy.readFile(); 
	 }

}

package ro.pentastagiu.parkingapp.service;

import ro.pentastagiu.parkingapp.dao.JDBMySQLConnection;
import ro.pentastagiu.parkingapp.model.Ticket;

public class TicketServiceImpl implements TicketService {

	public Ticket createTicket() {
		return null;

	}

	/**
	 * Returns true if it is a valid ticket and false otherwise.
	 * 
	 * @param ticketCode
	 * @return
	 */
	public boolean validateTicket(String ticketCode) {
		boolean ticketFound = false;
		ticketFound = JDBMySQLConnection.checkUUID(ticketCode);
		if (ticketFound)
			return true;
		else
			return false;

	}

}

package ro.pentastagiu.parkingapp.service;

import ro.pentastagiu.parkingapp.model.VehicleType;

public interface PaymentService {

	public long computePayment(VehicleType type, String ticketCode, Context context);

}
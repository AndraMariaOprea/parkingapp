package ro.pentastagiu.parkingapp.service;

import ro.pentastagiu.parkingapp.model.VehicleType;

public class PaymentServiceImpl implements PaymentService {

	public long computePayment(VehicleType type, String ticketCode, Context context) {
		ParkingService parkingService = new ParkingServiceImpl();
		long period = (parkingService.calculateParkingDuration(ticketCode)) / 1000; // seconds
		long parkedPeriodHours = period / 3600; // hour

		Integer price = context.executePaymentStrategy().get(type);

		System.out.println("price:" + price);
		long priceL = price.longValue();

		return priceL * parkedPeriodHours;

	}

}
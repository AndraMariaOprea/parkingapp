package ro.pentastagiu.parkingapp.service;

import java.util.Map;

import ro.pentastagiu.parkingapp.model.ReadFiles;
import ro.pentastagiu.parkingapp.model.VehicleType;

public class PaymentDayStrategy implements PaymentStrategy{

	
	@Override
	public Map<VehicleType, Integer> readFile() {
		ReadFiles read = new ReadFiles();
		Map<VehicleType, Integer> priceMap = read.readPricesFromFile("pricesDay.txt");
		return priceMap;
	}

}

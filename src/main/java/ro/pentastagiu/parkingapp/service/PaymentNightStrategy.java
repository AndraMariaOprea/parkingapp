package ro.pentastagiu.parkingapp.service;

import java.util.Map;

import ro.pentastagiu.parkingapp.model.ReadFiles;
import ro.pentastagiu.parkingapp.model.VehicleType;

public class PaymentNightStrategy implements PaymentStrategy{

	@Override
	public Map<VehicleType, Integer> readFile() {
		//System.out.println(file + "night strategy");
		ReadFiles read = new ReadFiles();
		Map<VehicleType, Integer> priceMap = read.readPricesFromFile("pricesNight.txt");
		return priceMap;
	}
}

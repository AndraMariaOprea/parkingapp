package ro.pentastagiu.parkingapp.service;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Observable;

import ro.pentastagiu.parkingapp.model.ParkingSpace;
import ro.pentastagiu.parkingapp.model.ReadFiles;
import ro.pentastagiu.parkingapp.model.Ticket;
import ro.pentastagiu.parkingapp.model.VehicleType;
import ro.pentastagiu.parkingapp.view.DisplayPanel;
import ro.pentastagiu.parkingapp.view.GateUI;

public class ParkingFacade {
	private ArrayList<ParkingSpace> spacesList;
	
	private ParkingService parkingService;
	
	private PaymentService paymentService;
	
	private TicketService ticketService;
	
	public ArrayList<ParkingSpace> initializeParking(ArrayList<ParkingSpace> spacesList) throws FileNotFoundException{
		ReadFiles readFiles = new ReadFiles();
		spacesList = (ArrayList<ParkingSpace>) readFiles.readParkingSpaces("parkingSpaces.txt");
		return spacesList;
	}
	public ParkingFacade() throws FileNotFoundException{
		this.parkingService = new ParkingServiceImpl(initializeParking(spacesList));
		this.paymentService = new PaymentServiceImpl();
		this.ticketService = new TicketServiceImpl();
	}
	/**
	 * Process vehicle
	 */
	public Ticket executeParking(){
		
		ParkingSpace convertor = new ParkingSpace();
		convertor.convertStringToVehicleType(GateUI.selectVehicleType());
		
		Ticket ticketStatus =  parkingService.processVehicle(convertor.getSpaceType());
		return ticketStatus;
	}
	
	public void addDisplayPanel(){
		DisplayPanel displayPanel = new DisplayPanel();
		((Observable) parkingService).addObserver(displayPanel);
	}
	
	public boolean validateTicket(String ticketCode){
		return ticketService.validateTicket(ticketCode);
	}
	
	public VehicleType findSpaceType(String ticketCode) {
		
		
		return parkingService.findSpaceType(ticketCode);	
	}
	
	public Long computePayment(String ticketCode, Context context) {
		
		return paymentService.computePayment(findSpaceType(ticketCode), ticketCode, context);
		
	}
}

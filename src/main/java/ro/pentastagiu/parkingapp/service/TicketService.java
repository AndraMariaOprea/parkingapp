package ro.pentastagiu.parkingapp.service;

public interface TicketService {
	public boolean validateTicket(String ticketCode);

}

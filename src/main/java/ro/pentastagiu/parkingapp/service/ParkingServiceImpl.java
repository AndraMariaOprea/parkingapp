package ro.pentastagiu.parkingapp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Observable;

import ro.pentastagiu.parkingapp.dao.JDBMySQLConnection;
import ro.pentastagiu.parkingapp.model.ParkingLot;
import ro.pentastagiu.parkingapp.model.ParkingSpace;
import ro.pentastagiu.parkingapp.model.Ticket;
import ro.pentastagiu.parkingapp.model.VehicleType;

public class ParkingServiceImpl extends Observable implements ParkingService {

	ParkingLot parkingLot;
	ParkingSpace parkingSpace;

	public ParkingServiceImpl() {
		parkingLot = new ParkingLot();
	}

	public ParkingServiceImpl(ParkingLot parkingLot) {
		this.parkingLot = parkingLot;
	}

	public ParkingServiceImpl(ArrayList<ParkingSpace> freeSpacesList) {
		parkingLot = new ParkingLot(freeSpacesList);
	}

	public void setParkingLot(ParkingLot parkingLot) {
		this.parkingLot = parkingLot;
	}

	public ParkingLot getParkingLot() {
		return parkingLot;
	}

	public ParkingSpace getParkingSpace() {
		return parkingSpace;
	}

	/**
	 * Returns the ticket according to the type of vehicle entered by the user.
	 * Notify display panel at every change.
	 */
	public Ticket processVehicle(VehicleType vehicleType) {
		Ticket ticket = new Ticket();
		for (Iterator<ParkingSpace> iterator = parkingLot.getFreeParkingSpaces().iterator(); iterator.hasNext();) {
			ParkingSpace parkingspace = iterator.next();
			if (parkingspace.getSpaceType().equals(vehicleType)) {
				ticket.getpSpace().setVehicleType(vehicleType);
				ticket.setpSpace(parkingspace);
				parkingLot.getOccupiedSpaces().add(parkingspace);
				parkingLot.getFreeParkingSpaces().remove(iterator.next());
				setChanged();
				notifyObservers();
				break;
			}
		}
		return ticket;
	}

	/**
	 * Returns vehicle type
	 */
	public VehicleType typeOfVehicle(int parkingSpaceID) {
		VehicleType type = null;
		for (Iterator<ParkingSpace> iterator = parkingLot.getFreeParkingSpaces().iterator(); iterator.hasNext();) {
			ParkingSpace parkingspace = iterator.next();
			if (parkingspace.getSpaceID() == parkingSpaceID) {
				type = parkingspace.getSpaceType();
				break;
			}
		}
		return type;
	}

	/**
	 * Returns parking duration
	 */
	public long calculateParkingDuration(String ticketCode) {
		Calendar timeNow = Calendar.getInstance();
		System.out.println("C1:" + timeNow.getTimeInMillis());
		Timestamp timeFromDB = JDBMySQLConnection.extractTime(ticketCode); // begin
																			// time
		Calendar begin = Calendar.getInstance();
		begin.setTimeInMillis(timeFromDB.getTime());
		System.out.println("C2:" + begin.getTimeInMillis());
		long period = (timeNow.getTimeInMillis() - begin.getTimeInMillis());

		return period;

	}

	public VehicleType findSpaceType(String ticketCode) {
		int parkingSpaceID = JDBMySQLConnection.extractParkingSpace(ticketCode);
		VehicleType spaceType = typeOfVehicle(parkingSpaceID);
		return spaceType;
	}

}
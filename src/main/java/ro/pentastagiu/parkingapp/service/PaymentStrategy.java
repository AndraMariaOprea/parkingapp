package ro.pentastagiu.parkingapp.service;

import java.util.Map;

import ro.pentastagiu.parkingapp.model.VehicleType;

public interface PaymentStrategy {
	public Map<VehicleType, Integer> readFile();

}

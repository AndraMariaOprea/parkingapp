package ro.pentastagiu.parkingapp.entity;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerSingleton {
	//@PersistenceContext
	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;
	
	@Produces
	public static EntityManager getEntityManager(){
		if(entityManager==null){
			entityManagerFactory = Persistence.createEntityManagerFactory("parkingapp-pu");
			entityManager = entityManagerFactory.createEntityManager();
		}
		return entityManager;
	}
	public void destroy( EntityManagerFactory factory){
		factory.close();
	} 
	
	/*public static EntityManagerFactory getFactory(){
		return entityManagerFactory;
	}
	public static synchronized EntityManagerFactory getInstance() {
		if (entityManagerFactory == null) {
			entityManagerFactory = Persistence.createEntityManagerFactory("parkingapp-pu");
		}
		return entityManagerFactory;
		}
	
	public  EntityManager getEntityManager(){
		return getFactory().createEntityManager(); 
	}
*/
}
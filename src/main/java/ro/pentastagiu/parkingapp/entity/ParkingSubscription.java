package ro.pentastagiu.parkingapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
//@Table(name="PARKING_SUBSCRIPTIONS")
public class ParkingSubscription {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String parkingSubscriptionType;
	
	@ManyToOne
	private Customer customer;
	
	public ParkingSubscription(){
		
	}
	
	public ParkingSubscription(Long id, String parkingSubscriptionType, Customer customer) {
		super();
		this.id = id;
		this.parkingSubscriptionType = parkingSubscriptionType;
		this.customer = customer;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getParkingSubscriptionType() {
		return parkingSubscriptionType;
	}
	public void setParkingSubscriptionType(String parkingSubscriptionType) {
		this.parkingSubscriptionType = parkingSubscriptionType;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "ParkingSubscription [id=" + id + ", parkingSubscriptionType=" + parkingSubscriptionType + ", customer="
				+ customer + "]";
	}


}

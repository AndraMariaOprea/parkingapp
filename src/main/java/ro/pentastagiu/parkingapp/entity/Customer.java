package ro.pentastagiu.parkingapp.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
//@Table(name="CUSTOMERS")
public class Customer {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	@OneToMany (mappedBy = "customer")
	private List<ParkingSubscription> parkingSubscription;
	
	public Customer(){
		
	}

	public Customer(int id, String name) {
		super();
		this.id = (long) id;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ParkingSubscription> getParkingSubscriptions() {
		return parkingSubscription;
	}
	public void setParkingSubscriptions(List<ParkingSubscription> parkingSubscriptions) {
		this.parkingSubscription = parkingSubscriptions;
	}

	@Override
	public String toString() {
		return "Customer: "  + "Name:" + name + "," + "id=" + id ;
	}

}

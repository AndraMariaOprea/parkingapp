package ro.pentastagiu.parkingapp.entity;

import javax.inject.Inject;
import javax.inject.Named;

import ro.pentastagiu.parkingapp.dao.CustomerDAO;
import ro.pentastagiu.parkingapp.dao.ParkingSubscriptionDAO;
import ro.pentastagiu.parkingapp.dao.ParkingSubscriptionDAOImpl;

public class MainJPA {
	@Inject
	@Named("customerDAO")
	private static CustomerDAO customerDAO;
	@Inject
	private static ParkingSubscriptionDAO parkingSubscriptionDAO;

	@Inject
	private Customer customer;

	public static void testCustomerDAO()

	{
		// CustomerDAO customerDAO = new CustomerDAO();

		Customer customer1 = new Customer();
		customer1.setName("Alina");
		Customer customer2 = new Customer();
		customer2.setName("Vasile");
		Customer customer3 = new Customer();
		customer3.setName("Eugen");

		// CREATE
		customerDAO.insertCustomer(customer1);
		customerDAO.insertCustomer(customer2);
		customerDAO.insertCustomer(customer3);

		// READ
		Customer readCustomer = customerDAO.getCustomer((long) 2);
		System.out.println(readCustomer);

		// UPDATE
		Customer updateCustomer = new Customer(1, "Vasile");
		customerDAO.updateCustomer(updateCustomer);

		// DELETE
		customerDAO.deleteCustomer((long) 3);
	}

	public static void testParkingSubscriptionDAO() {

		ParkingSubscriptionDAOImpl parkingSubscriptionDAO = new ParkingSubscriptionDAOImpl();

		ParkingSubscription pSubscribtion1 = new ParkingSubscription();
		pSubscribtion1.setParkingSubscriptionType("SMALL");
		Customer customer1 = new Customer();
		customer1.setName("Alina");
		pSubscribtion1.setCustomer(customer1);

		ParkingSubscription pSubscribtion2 = new ParkingSubscription();
		pSubscribtion2.setParkingSubscriptionType("MEDIUM");
		pSubscribtion2.setCustomer(customer1);
		ParkingSubscription pSubscribtion3 = new ParkingSubscription();
		pSubscribtion3.setParkingSubscriptionType("LARGE");
		pSubscribtion3.setCustomer(customer1);

		// CREATE
		parkingSubscriptionDAO.insertParkingSubscription(pSubscribtion1);
		parkingSubscriptionDAO.insertParkingSubscription(pSubscribtion2);
		parkingSubscriptionDAO.insertParkingSubscription(pSubscribtion3);

		// READ
		ParkingSubscription readParkingSubscription = parkingSubscriptionDAO.getParkingSubscription((long) 1);
		System.out.println(readParkingSubscription);
		/*
		 * // UPDATE ParkingSubscription updateParkingSubscription = new
		 * ParkingSubscription(1, "SMALL",customer1);
		 * parkingSubscriptionDAO.updateParkingSubscription(
		 * updateParkingSubscription);
		 * 
		 * // DELETE parkingSubscriptionDAO.deleteParkingSubscription((long) 3);
		 * 
		 */
	}

	public static void main(String[] args) {
		// CustomerDAO customerDAO = new CustomerDAO();

		// ParkingSubscriptionDAOImpl parkingSubscriptionDAO = new
		// ParkingSubscriptionDAOImpl();

		Customer customer1 = new Customer();
		customer1.setName("Alina");
		customerDAO.insertCustomer(customer1);

		Customer customer2 = new Customer();
		customer2.setName("Vasile");
		Customer customer3 = new Customer();
		customer3.setName("Eugen");

		ParkingSubscription pSubscribtion1 = new ParkingSubscription();
		pSubscribtion1.setParkingSubscriptionType("SMALL");
		pSubscribtion1.setCustomer(customer1);

		parkingSubscriptionDAO.insertParkingSubscription(pSubscribtion1);

	}
}

package ro.pentastagiu.parkingapp.dao;

import ro.pentastagiu.parkingapp.entity.ParkingSubscription;

public interface ParkingSubscriptionDAO{
	
	ParkingSubscription getParkingSubscription(Long id);

	void insertParkingSubscription(ParkingSubscription parkingSubscription);

	void updateParkingSubscription(ParkingSubscription parkingSubscription);

	void deleteParkingSubscription(Long id);

}

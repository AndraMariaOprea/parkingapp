package ro.pentastagiu.parkingapp.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;

public class JDBMySQLConnection {

	public static Connection connection = null;

	public static String url;
	public static String user;
	public static String password;

	public static void getDbInfo() {
		Properties properties = new Properties();
		InputStream input = null;

		try {
			input = JDBMySQLConnection.class.getClassLoader().getResourceAsStream("dbconfig.properties");
			properties.load(input);

			url = properties.getProperty("URL");
			user = properties.getProperty("USER");
			password = properties.getProperty("PASSWORD");

			properties = null;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Connection getDatabaseConnection() {
		try {
			getDbInfo();
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(url, user, password);
			return connection;
		} catch (Exception e) {
			try {
				connection.close();
				connection = null;
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return null;

	}

	public static boolean addTicket(Timestamp entryDate, String uuid, int parkingSpaceID) {

		int i = 0;
		PreparedStatement statement = null;
		try {
			connection = getDatabaseConnection();
			if (connection != null) {
				String sql = "INSERT INTO tickets(entryDate, uuid, parkingSpaceID, paidTicket, exitDate) VALUES(?,?,?,'0',NULL)";

				statement = connection.prepareStatement(sql);
				statement.setTimestamp(1, entryDate);
				statement.setString(2, uuid);
				statement.setInt(3, parkingSpaceID);

				i = statement.executeUpdate();
				System.out.println("Data Added Successfully");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (i > 0) {
			return true;
		} else
			return false;

	}

	/**
	 * Check if the entered code/uuid is in the database. Returns true if the
	 * code is in database and false if it is not.
	 * 
	 * @param uuid
	 * @return
	 */
	public static boolean checkUUID(String uuid) {
		PreparedStatement statement = null;
		try {
			connection = getDatabaseConnection();
			statement = connection.prepareStatement("select uuid from tickets where uuid = ?");
			statement.setString(1, uuid);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) // found
			{
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
			return false;
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public static Timestamp extractTime(String uuid) {
		PreparedStatement statement = null;
		Timestamp entryDate = null;
		try {
			connection = getDatabaseConnection();
			statement = connection.prepareStatement("select * from tickets where uuid = ?");
			statement.setString(1, uuid);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) // found
			{
				entryDate = resultSet.getTimestamp("entryDate");

			} else {
				System.out.println("Not found this code");
			}
		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return entryDate;
	}

	/**
	 * Returns the parking space id depending on the ticket uuid.
	 * 
	 * @param uuid
	 * @return
	 */
	public static int extractParkingSpace(String uuid) {
		PreparedStatement statement = null;
		int parkingSpaceID = 0;
		try {
			connection = getDatabaseConnection();
			statement = connection.prepareStatement("select * from tickets where uuid = ?");
			statement.setString(1, uuid);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) // found
			{
				parkingSpaceID = resultSet.getInt("parkingSpaceID");

			} else {
				System.out.println("Not found this code");
			}
		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return parkingSpaceID;

	}

	public static void updatePaidTicketField(String uuid, Timestamp exitDate) {
		PreparedStatement statement = null;
		try {
			connection = getDatabaseConnection();
			statement = connection.prepareStatement("UPDATE tickets set paidTicket = 1, exitDate = ?  WHERE uuid = ? ");
			statement.setTimestamp(1, exitDate);
			statement.setString(2, uuid);

			statement.executeUpdate();

			System.out.println("Data Updated Successfully");

		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void deleteTicket(String uuid) {
		PreparedStatement statement = null;
		try {
			connection = getDatabaseConnection();
			statement = connection.prepareStatement("DELETE from tickets  WHERE uuid = ? ");
			statement.setString(1, uuid);
			statement.executeUpdate();

			System.out.println("Ticket successfully deleted.");

		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static int countRowsOfTable() {
		PreparedStatement statement = null;
		int rowCount = 0;
		try {
			connection = getDatabaseConnection();
			statement = connection.prepareStatement("select count(*) from tickets");
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) // found
			{
				rowCount = resultSet.getInt(1);

			} else {
				System.out.println("Not counted");
			}
		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return rowCount;

	}
}

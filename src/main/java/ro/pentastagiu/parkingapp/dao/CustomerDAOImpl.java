package ro.pentastagiu.parkingapp.dao;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ro.pentastagiu.parkingapp.entity.Customer;


@Named("customerDAO")

public class CustomerDAOImpl implements CustomerDAO {
	
	@Inject
	@PersistenceContext
	private EntityManager entityManager;
	
	@Inject 
	private Customer customer;
	
	
	public Customer getCustomer(Long id) {
		

		
			customer = entityManager.find(Customer.class, id);

		
		return customer;
	}
	
	public String getGreeting(){
		return "Hello!! CDi";
	}
	public void insertCustomer(Customer customer){
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(customer);
			entityManager.getTransaction().commit();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateCustomer(Customer customer){
		try{
			entityManager.getTransaction().begin();
			entityManager.merge(customer);
			entityManager.getTransaction().commit();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteCustomer(Long id) {
		try {
			entityManager.getTransaction().begin();
			Customer customer = entityManager.find(Customer.class, id);
			entityManager.remove(customer);
			entityManager.getTransaction().commit();
		}catch (Exception e) {
			e.printStackTrace();
		}
		

	}
	public int number(int n){
		return n;
	}

}

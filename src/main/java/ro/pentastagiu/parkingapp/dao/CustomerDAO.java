package ro.pentastagiu.parkingapp.dao;

import ro.pentastagiu.parkingapp.entity.Customer;

public interface CustomerDAO {

	Customer getCustomer(Long id);

	void insertCustomer(Customer customer);

	void updateCustomer(Customer customer);

	void deleteCustomer(Long id);

	int number(int n);

	String getGreeting();

}

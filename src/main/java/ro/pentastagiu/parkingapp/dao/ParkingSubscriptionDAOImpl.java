package ro.pentastagiu.parkingapp.dao;

import javax.persistence.EntityManager;

import ro.pentastagiu.parkingapp.entity.ParkingSubscription;

public class ParkingSubscriptionDAOImpl implements ParkingSubscriptionDAO {

	EntityManager entityManager;
	
	public ParkingSubscription getParkingSubscription(Long id) {
		
		ParkingSubscription parkingSubscription = null;

		try {
			parkingSubscription = entityManager.find(ParkingSubscription.class, id);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return parkingSubscription;
	}

	public void insertParkingSubscription(ParkingSubscription parkingSubscription) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(parkingSubscription);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateParkingSubscription(ParkingSubscription parkingSubscription) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(parkingSubscription);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteParkingSubscription(Long id) {
		try {
			entityManager.getTransaction().begin();
			ParkingSubscription parkingSubscription = entityManager.find(ParkingSubscription.class, id);
			entityManager.remove(parkingSubscription);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

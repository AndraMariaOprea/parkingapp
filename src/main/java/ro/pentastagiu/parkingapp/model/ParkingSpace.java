package ro.pentastagiu.parkingapp.model;

public class ParkingSpace {

	private int spaceID;
	private VehicleType vehicleType;
	private String spaceNumber;
	private String floor;

	public ParkingSpace() {

	}
	public ParkingSpace(int spaceID, VehicleType vehicleType, String spaceNumber, String floor) {
		
		this.spaceID = spaceID;
		this.vehicleType = vehicleType;
		this.spaceNumber = spaceNumber;
		this.floor = floor;
	}
	
	public int getSpaceID() {
		return spaceID;
	}

	public VehicleType getSpaceType() {
		return vehicleType;
	}

	public String getSpaceNumber() {
		return spaceNumber;
	}

	public String getFloor() {
		return floor;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	public void setSpaceNumber(String spaceNumber) {
		this.spaceNumber = spaceNumber;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}
/**
 * Convert user input from String to VehicleType
 * @param vehicleType
 */
	public void convertStringToVehicleType(String vehicleType) {
		switch (vehicleType.toUpperCase()) {
		case "SMALL":
			setVehicleType(VehicleType.SMALL);
			break;
		case "MEDIUM":
			setVehicleType(VehicleType.MEDIUM);
			break;
		case "LARGE":
			setVehicleType(VehicleType.LARGE);
			break;
		default:
			System.out.println("Not a valid vehicle type!");
			break;
		}
	}

	@Override
	public String toString() {
		return "ParkingSpace [spaceID=" + spaceID + ", vehicleType=" + vehicleType + ", spaceNumber=" + spaceNumber
				+ ", floor=" + floor + "]";
	}	

}
package ro.pentastagiu.parkingapp.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ReadFiles {

	private Map<VehicleType, Integer> priceMap;
	ParkingSpace space = new ParkingSpace();
	/**
	 * Read prices from file
	 * @param fileName
	 * @return
	 */
	public Map<VehicleType, Integer> readPricesFromFile(String fileName) {

		priceMap = new HashMap<VehicleType, Integer>();
		try {
			// Get file from resources folder
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(fileName).getFile());

			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split(",");
				Integer price = Integer.parseInt(parts[1]);
				String spaceType = parts[0];
				space.convertStringToVehicleType(spaceType);
				priceMap.put(space.getSpaceType(), price);
}
			reader.close();

		} catch (IOException e) {
			System.err.println(e);
		}
		return priceMap;

	}

	public List<ParkingSpace> readParkingSpaces(String fileName) throws FileNotFoundException {

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		Scanner input = new Scanner(file);
		input.useDelimiter(",|\n");

		List<ParkingSpace> parkingSpaces = new ArrayList<ParkingSpace>();

		while (input.hasNext()) {

			int spaceID = input.nextInt();
			String spaceType = input.next();
			String spaceNumber = input.next();
			String floor = input.next();
			space.convertStringToVehicleType(spaceType);
			ParkingSpace newProduct = new ParkingSpace(spaceID, space.getSpaceType(), spaceNumber, floor);

			parkingSpaces.add(newProduct);

		}
		input.close();
		return parkingSpaces;
	}

	public void displayNumberOfSpaces(ArrayList<ParkingSpace> parkingSpaces) {

		long smallSpaces = parkingSpaces.stream()
				.filter(spacetype -> spacetype.getSpaceType().equals(VehicleType.SMALL)).count();
		long mediumSpaces = parkingSpaces.stream()
				.filter(spacetype -> spacetype.getSpaceType().equals(VehicleType.MEDIUM)).count();
		long largeSpaces = parkingSpaces.stream()
				.filter(spacetype -> spacetype.getSpaceType().equals(VehicleType.LARGE)).count();
		System.out.println("TOTAL spaces:" + parkingSpaces.size() + "\n" + "No of SMALL spaces:" + smallSpaces + "\n"
				+ "No of MEDIUM spaces:" + mediumSpaces + "\n" + "No of LARGE spaces:" + largeSpaces + "\n");

	}
}
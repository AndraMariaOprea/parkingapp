package ro.pentastagiu.parkingapp.model;

public enum VehicleType {
	
	SMALL, MEDIUM, LARGE;
	
}
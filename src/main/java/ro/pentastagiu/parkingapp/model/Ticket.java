package ro.pentastagiu.parkingapp.model;

import java.util.Calendar;
import java.util.UUID;

public class Ticket {
	private Calendar entryDate;
	private Calendar exitDate;
	private String uuid;
	private ParkingSpace parkingSpace;

	public Ticket() {
		this.uuid = UUID.randomUUID().toString();
		this.entryDate = Calendar.getInstance();
		this.parkingSpace = new ParkingSpace();
	}

	public Calendar getDate() {
		return entryDate;
	}

	public String getUuid() {
		return uuid;
	}

	public ParkingSpace getpSpace() {
		return parkingSpace;
	}

	public Calendar getEntrydate() {
		return entryDate;
	}

	public void setEntrydate(Calendar entrydate) {
		this.entryDate = entrydate;
	}

	public Calendar getExitdate() {
		return exitDate;
	}

	public void setExitdate(Calendar exitdate) {
		this.exitDate = exitdate;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setpSpace(ParkingSpace pSpace) {
		this.parkingSpace = pSpace;
	}

	@Override
	public String toString() {
		return "Ticket [entrydate=" + entryDate + ", uuid=" + uuid + ", pSpace=" + parkingSpace + "]";
	}
}
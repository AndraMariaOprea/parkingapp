package ro.pentastagiu.parkingapp.model;

import java.util.ArrayList;

public class ParkingLot {

	private ArrayList<ParkingSpace> freeSpacesList= new ArrayList<ParkingSpace>();
	private ArrayList<ParkingSpace> occupiedSpacesList = new ArrayList<ParkingSpace>() ;

	private Ticket ticket;

	public ParkingLot() {

	}

	public ParkingLot(ArrayList<ParkingSpace> freeSpacesList) {
		this.freeSpacesList = freeSpacesList;

	}
	public ArrayList<ParkingSpace> getFreeParkingSpaces() {
		return freeSpacesList;
	}

	public void setFreeParkingSpaces(ArrayList<ParkingSpace> parkingSpaces) {
		this.freeSpacesList = parkingSpaces;
	}

	public ParkingLot(Ticket ticket) {
		this.ticket = ticket;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public ArrayList<ParkingSpace> getOccupiedSpaces() {
		return occupiedSpacesList;
	}

	public void setOccupiedSpaces(ArrayList<ParkingSpace> occupiedSpaces) {
		this.occupiedSpacesList = occupiedSpaces;
	}
	
	@Override
	public String toString() {
		return "ParkingLot [freeSpacesList=" + freeSpacesList + ", occupiedSpacesList=" + occupiedSpacesList
				+  ", ticket=" + ticket + "]";
	}

}
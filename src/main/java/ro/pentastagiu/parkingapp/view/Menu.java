package ro.pentastagiu.parkingapp.view;

import java.io.FileNotFoundException;

public class Menu {
	private static boolean run = true;

	public static void main(String[] args) throws FileNotFoundException {

		int menuOption;
		System.out.println("Welcome to parking app!");

		while (run) {
			menuOption = GateUI.showMenuAndEnterChoice();
			Gate gate = new Gate();
			gate.processOption(menuOption);
		}

		System.out.println("!STOP!");

	}
	
}
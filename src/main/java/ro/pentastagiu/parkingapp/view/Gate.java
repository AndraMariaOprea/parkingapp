package ro.pentastagiu.parkingapp.view;

import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.util.Calendar;

import ro.pentastagiu.parkingapp.dao.JDBMySQLConnection;
import ro.pentastagiu.parkingapp.model.Ticket;
import ro.pentastagiu.parkingapp.service.Context;
import ro.pentastagiu.parkingapp.service.ParkingFacade;
import ro.pentastagiu.parkingapp.service.PaymentDayStrategy;
import ro.pentastagiu.parkingapp.service.PaymentNightStrategy;

public class Gate {

	public void processOption(int option) throws FileNotFoundException {

		ParkingFacade parkingFacade = new ParkingFacade();

		boolean run = true;

		parkingFacade.addDisplayPanel();

		switch (option) {
		case 1:

			Ticket ticketStatus = parkingFacade.executeParking();
			Timestamp entry = new Timestamp(ticketStatus.getEntrydate().getTimeInMillis());
			JDBMySQLConnection.addTicket(entry, ticketStatus.getUuid(), ticketStatus.getpSpace().getSpaceID());
			GateUI.displayTicket(ticketStatus);
			break;

		case 2:

			boolean ticketFound = false;
			String ticketCode = GateUI.enterTheTicketCode();

			ticketFound = parkingFacade.validateTicket(ticketCode);

			if (ticketFound) {

				GateUI.findTheTicket();
				/*
				 * long fee = parkingFacade.computePayment(ticketCode);
				 * GateUI.displayHowMuchToPay(fee);
				 */

				// Test Strategy
				Calendar timeNow = Calendar.getInstance();
				Calendar night = Calendar.getInstance();
				night.set(Calendar.HOUR, 6);
				if (timeNow.before(night)) { // DAY
					Context context = new Context(new PaymentDayStrategy());
					long fee = parkingFacade.computePayment(ticketCode, context);
					GateUI.displayHowMuchToPay(fee);
				} else {// NIGHT
					Context context = new Context(new PaymentNightStrategy());
					long fee = parkingFacade.computePayment(ticketCode, context);
					GateUI.displayHowMuchToPay(fee);
				}

				JDBMySQLConnection.deleteTicket(ticketCode);
				ticketFound = true;
				break;
			}
			if (!ticketFound) {
				GateUI.displayAnProblemOccured();
			}
			break;

		case 0:
			run = false;
		default:
			break;
		}
	}
}

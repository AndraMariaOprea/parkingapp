package ro.pentastagiu.parkingapp.view;

import java.util.InputMismatchException;
import java.util.Scanner;

import ro.pentastagiu.parkingapp.model.Ticket;

public class GateUI {

	public static String selectVehicleType() {
		String selection;
		Scanner input = new Scanner(System.in);
		
			System.out.println("Choose your vehicle type from these choices:");
			System.out.println("SMALL");
			System.out.println("MEDIUM");
			System.out.println("LARGE");

			selection = input.next();
			return selection;
		} 

	

	public static void displayTicket(Ticket ticketStatus) {

		System.out.println("Your ticket is:" + ticketStatus);
	}

	public static void displayHowMuchToPay(Long fee) {

		System.out.println("You have to pay: " + fee + "lei");
		System.out.println("Thank you!");
	}

	public static void displayAnProblemOccured() {

		System.out.println("An problem occured! We cannot find your ticket.");
	}

	/**
	 * Returns the ticket code entered by the user. More exactly returns the
	 * code used for validation.
	 * 
	 * @return
	 */
	public static String enterTheTicketCode() {
		System.out.println("Enter your ticket code, please: ");
		Scanner scanner = new Scanner(System.in);
		String ticketCode = scanner.next();
		return ticketCode;

	}

	public static void findTheTicket() {

		System.out.println("Hurry, we find the ticket,get the cash");
	}
	public static int showMenuAndEnterChoice() {
		Scanner scan = new Scanner(System.in);
		int option = 3;

		System.out.println("1. entry");
		System.out.println("2. exit");

		System.out.print("enter your choice:");
		try {
			option = scan.nextInt();
		} catch (InputMismatchException exception) {
			System.out.println("Integers only, please.");
			scan.next();
		}
		return option;
	}

	
	}
	

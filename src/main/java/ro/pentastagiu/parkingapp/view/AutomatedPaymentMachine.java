package ro.pentastagiu.parkingapp.view;

import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import ro.pentastagiu.parkingapp.dao.JDBMySQLConnection;
import ro.pentastagiu.parkingapp.model.ParkingSpace;
import ro.pentastagiu.parkingapp.model.ReadFiles;
import ro.pentastagiu.parkingapp.model.VehicleType;
import ro.pentastagiu.parkingapp.service.ParkingServiceImpl;
import ro.pentastagiu.parkingapp.service.PaymentService;
import ro.pentastagiu.parkingapp.service.PaymentServiceImpl;
import ro.pentastagiu.parkingapp.service.TicketService;
import ro.pentastagiu.parkingapp.service.TicketServiceImpl;

public class AutomatedPaymentMachine {
	private static ArrayList<ParkingSpace> spacesList;
	static ParkingServiceImpl parkingService;
	
	static VehicleType spaceType = null;
	public static void main(String []args) throws FileNotFoundException{
		ReadFiles readFiles = new ReadFiles();
		ParkingServiceImpl parkingService;
		spacesList = (ArrayList<ParkingSpace>) readFiles.readParkingSpaces("parkingSpaces.txt");
		parkingService = new ParkingServiceImpl(spacesList);
		
		boolean ticketFound = false;
		String ticketCode = GateUI.enterTheTicketCode();
		TicketService ticketValidation = new TicketServiceImpl();
		ticketFound = ticketValidation.validateTicket(ticketCode);

		if (ticketFound) {
			GateUI.findTheTicket();
			Calendar now = Calendar.getInstance();
			Timestamp nowPaymentMachine = new Timestamp(now.getTimeInMillis());//time when pay at PaymentMachine
			PaymentService payment = new PaymentServiceImpl();

			/*long fee = payment.computePayment(parkingService.findSpaceType(ticketCode), ticketCode);
			GateUI.displayHowMuchToPay(fee);*/
			
			//Update Database ; paidTicket=1, exitDate = Time now
			JDBMySQLConnection.updatePaidTicketField( ticketCode, nowPaymentMachine);
			
		
	}
	
	}
}




package ro.pentastagiu.parkingapp.view;

import java.util.Observable;
import java.util.Observer;

import ro.pentastagiu.parkingapp.service.ParkingServiceImpl;

public class DisplayPanel implements Observer {

	@Override
	public void update(Observable parkingLotService, Object getFreeParkingSpaces) {
		System.out.println("---  Display Panel ---");
		ParkingServiceImpl parkingService = (ParkingServiceImpl) parkingLotService;
		parkingService.getParkingLot().getFreeParkingSpaces().forEach(parkingSpace -> System.out.println(parkingSpace));
		System.out.println("--- End Display Panel ---");
	}

}

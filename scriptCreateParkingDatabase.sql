CREATE database parking;
USE parking;

CREATE TABLE tickets (
	ticketID INT NOT NULL AUTO_INCREMENT,
	entryDate TIMESTAMP NOT NULL,
	exitDate TIMESTAMP NULL,
	uuid VARCHAR(100) NOT NULL,
	parkingSpaceID INTEGER NOT NULL,
	paidTicket BOOLEAN,
	PRIMARY KEY (ticketID)
	);
	
